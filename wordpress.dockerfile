FROM wordpress:5.2.1-php7.3-apache

RUN apt-get update -y && apt-get install -y --no-install-recommends git vim

RUN cd /etc/apache2/sites-enabled && ln -s ../sites-available/default-ssl.conf ./
